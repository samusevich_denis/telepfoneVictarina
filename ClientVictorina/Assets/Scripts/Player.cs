﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player
{
    public string Name;
    public int Score;
    public string IpAddress;
    
    public Player(string name)
    {
        IpAddress = string.Empty;
        Name = name;
        Score = 0;
    }
    public Player(string name, string iPAddress)
    {
        IpAddress = iPAddress;
        Name = name;
        Score = 0;
    }
    public Player(Player player)
    {
        IpAddress = player.IpAddress;
        Name = player.Name;
        Score = player.Score;
    }
}
