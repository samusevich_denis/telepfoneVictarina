﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Victorina.Network;

namespace Victorina.Admin
{
    public class AdminManager : MonoBehaviour
    {
        [SerializeField] private MainPanel mainPanel;
        public static AdminManager Instance;
        public AdminState AdminState;
        //public bool IsPlayGame => isPlayGame;
        //private bool isPlayGame;
        private List<Player> players = new List<Player>();

        private void Awake()
        {
            Instance = this;
        }
        private void Start()
        {
            KeeperUIPanels.Instance.GetPanel(UIPanelType.MainPanel).ShowPanel();
            NetworkServer.StarServer();
        }

        public void AddPlayer(Player player)
        {
            Debug.Log("Игрок добавлен");
            players.Add(player);
            mainPanel.AddPlayer(player.Name);
        }

        public void StartGame()
        {
            //msg = ClientMessageHandler.GetMessage();
            for (int i = 0; i < players.Count; i++)
            {
                NetworkServer.SendAllClientMessage("Start");
            }
            //isPlayGame = true;
        }

        //public void StopGame()
        //{
        //    isPlayGame = false;
        //}

        public void RemovePlayer(Player player)
        {
            var removePlayer = players.FirstOrDefault((c) => c.Name.Equals(player.Name, System.StringComparison.OrdinalIgnoreCase));
            players.Remove(removePlayer);
        }

        private void OnDestroy()
        {
            
        }
    }
}