﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Victorina.Admin
{
    public abstract class AdminState
    {
        public abstract void UpdateState(AdminManager game);

        public abstract void ChangeState(AdminManager game);
    }
}

