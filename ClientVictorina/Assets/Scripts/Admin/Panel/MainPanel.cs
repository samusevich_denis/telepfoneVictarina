﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Victorina.Network;
using Victorina.UI;

namespace Victorina.Admin
{
    public class MainPanel : UIPanel
    {
        [SerializeField] private ListPlayers listPlayers;
        [SerializeField] private Button startGame;
        [SerializeField] private Text Ip;
        protected override void Awake()
        {
            base.Awake();
            startGame.onClick.AddListener(StartGame);
            Ip.text = NetworkServer.LocalIPAddress();
        }

        private void StartGame()
        {
            AdminManager.Instance.StartGame();
        }


        private void OnDestroy()
        {
            startGame.onClick.RemoveListener(StartGame);
        }

        public void AddPlayer(string name)
        {
            listPlayers.AddPlayer(name);
        }
    }
}