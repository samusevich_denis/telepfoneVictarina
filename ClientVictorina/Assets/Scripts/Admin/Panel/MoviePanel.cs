﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Victorina.UI;

public class MoviePanel : UIPanel
{
    [SerializeField] protected Button buttonRight;
    [SerializeField] protected Button buttonWrong;
    [SerializeField] private AudioSource audioSourceButton;
    [SerializeField] private Image imageMovie;
    [SerializeField] private Text nameMovie;

    protected override void Awake()
    {
        buttonRight.onClick.AddListener(() => { ClosePanel(); PlayAudioButton(); });
        buttonWrong.onClick.AddListener(() => { ClosePanel(); PlayAudioButton(); });
        base.Awake();
    }

    private void PlayAudioButton()
    {
        audioSourceButton.Play();
    }

    private void OnDestroy()
    {
        buttonRight.onClick.RemoveAllListeners();
        buttonWrong.onClick.RemoveAllListeners();
    }

    public void SetDataPanel(MovieData movieData)
    {
        imageMovie.sprite = movieData.Sprite;
        nameMovie.text = movieData.NameMovie;
    }
}
