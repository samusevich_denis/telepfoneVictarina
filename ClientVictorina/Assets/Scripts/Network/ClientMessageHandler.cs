﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Victorina.Client;
using Victorina.Network;

public  class ClientMessageHandler : MonoBehaviour
{
    private static List<string> messageToClients = new List<string>();
    public static void AddMessage(string message)
    {
        messageToClients.Add(message);
    }
    private void Update()
    {
        for (int i = 0; i < messageToClients.Count; i++)
        {
            HandlMessage(messageToClients[i]);
            messageToClients.RemoveAt(i);
            i--;
        }
    }

    private static void HandlMessage(string msg)
    {
        Debug.LogWarning(msg);
        var nmsg = JsonUtility.FromJson<NetworkMessageToClient>(msg);
        switch (nmsg.Type)
        {
            case TypeMessageToClient.Movie:
                break;
            case TypeMessageToClient.Question:
                break;
            case TypeMessageToClient.Music:
                break;
            case TypeMessageToClient.SkipTurn:
                break;
            default:
                break;
        }
    }

    //public static string GetMessage(Player player, TypeMessageToClient type = TypeMessageToClient.PlayerData)
    //{
    //    var data = JsonUtility.ToJson(player);
    //    var nmsg = new NetworkMessageToClient(type, data);
    //    var msg = JsonUtility.ToJson(nmsg);
    //    return string.Empty;
    //}

}
