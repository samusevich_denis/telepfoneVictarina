﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Victorina.Network
{
    public static class NetworkServer
    {
        private static List<NetworkClient> clients = new List<NetworkClient>();
        private static TcpListener tcpListener;
        public static int port {get=>51111;}

        public static async void StarServer()
        {
            tcpListener = new TcpListener(IPAddress.Any, port);
            tcpListener.Start();
            try
            {
                while (true)
                {
                    AddClient(await tcpListener.AcceptTcpClientAsync());
                }
            }
            finally
            {
                tcpListener.Stop();
                for (int i = 0; i < clients.Count; i++)
                {
                    clients[i].Close();
                }
            }
        }


        private static async void AddClient(TcpClient client)
        {
            await Task.Yield();
            var networkClient = new NetworkClient(client);
            clients.Add(networkClient);
            Thread clientThread = new Thread(new ThreadStart(networkClient.ReceiveMessagesForServer));
            clientThread.Start();

        }

        public static void RemoveClient(string playerName)
        {
            var client = clients.FirstOrDefault(c => c.Player.Name == playerName);
            if (client != null)
                clients.Remove(client);
        }

        public static void SendAllClientMessage(string message)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            for (int i = 0; i < clients.Count; i++)
            {
                clients[i].Stream.Write(data, 0, data.Length);
            }
        }

        public static string LocalIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP += ip.ToString()+"\n";
                }
            }
            return localIP;
        }

    }
}


