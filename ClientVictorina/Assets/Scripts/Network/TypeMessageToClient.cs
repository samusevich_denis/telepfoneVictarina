﻿namespace Victorina.Network
{
    public enum TypeMessageToClient
    {
        Movie,
        Question,
        Music,
        SkipTurn,
    }
}