﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Victorina.Admin;
using Victorina.Network;

public class ServerMessageHandler : MonoBehaviour
{
    private static List<string> messageToServers = new List<string>();
    public static void AddMessage(string message)
    {
        messageToServers.Add(message);
    }

    private void Update()
    {
        for (int i = 0; i < messageToServers.Count; i++)
        {
            HandlMessage(messageToServers[i]);
            messageToServers.RemoveAt(i);
            i--;
        }
    }


    private static void HandlMessage(string msg)
    {
        Debug.LogWarning(msg);
        var nmsg = JsonUtility.FromJson<NetworkMessageToServer>(msg);
        switch (nmsg.Type)
        {
            case TypeMessageToServer.PlayerData:
                var player = JsonUtility.FromJson<Player>(nmsg.Data);
                AdminManager.Instance.AddPlayer(player);
                break;
            case TypeMessageToServer.PlayerOnClick:
                break;
            default:
                break;
        }


    }

    public static string GetMessage(Player player, TypeMessageToServer type = TypeMessageToServer.PlayerData)
    {
        var data = JsonUtility.ToJson(player);
        var nmsg = new NetworkMessageToServer(type, data);
        var msg = JsonUtility.ToJson(nmsg);
        return msg;
    }
}
