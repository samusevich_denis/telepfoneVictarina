﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using Victorina.Admin;

namespace Victorina.Network
{
    public class NetworkClient
    {
        public Player Player { get; private set; }
        public NetworkStream Stream { get; private set; }

        private TcpClient tcpClient;
        public NetworkClient(TcpClient tcpClient)
        {
            this.tcpClient = tcpClient;
        }
        public NetworkClient() {}
        private bool isCloseConect = true;

        public void ReceiveMessagesForServer()
        {
            try
            {
                Stream = tcpClient.GetStream();
                isCloseConect = false;
                string msg;
                while (true)
                {
                    try
                    {
                        msg = GetMessage();
                        if (msg.Equals(string.Empty, StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }
                        ServerMessageHandler.AddMessage(msg);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError(ex.Message);
                        break;
                    }
                }
            }
            finally
            {
                AdminManager.Instance.RemovePlayer(Player);
                NetworkServer.RemoveClient(this.Player.Name);
                Close();
            }
        }

        private string GetMessage()
        {
            StringBuilder builder = new StringBuilder();
            byte[] data = new byte[64];
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);
            return builder.ToString();
        }

        public void Close()
        {
            if (isCloseConect)
            {
                return;
            }
            isCloseConect = false;
            if (Stream != null)
            {
                Stream.Close();
            }
            if (tcpClient != null)
            {
                tcpClient.Close();
            }
        }


        public  void StartClient(Player player, IPAddress address)
        {
            Player = player;
            tcpClient = new TcpClient();

            try
            {
                tcpClient.Connect(address, NetworkServer.port); 
                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessagesForClient));
                receiveThread.Start(); 
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                Close();
            }
        }


        public void SendMessageToServer(string msg)
        {

            try
            {
                byte[] data = Encoding.Unicode.GetBytes(msg);
                Stream.Write(data, 0, data.Length);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                Close();
            }
        }

        private void ReceiveMessagesForClient()
        {
            try
            {
                Stream = tcpClient.GetStream();
                isCloseConect = false;
                string msg;
                while (true)
                {
                    try
                    {
                        msg = GetMessage();
                        if (msg.Equals(string.Empty, StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }
                        ClientMessageHandler.AddMessage(msg);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError(ex.Message);
                        break;
                    }
                }
            }
            finally
            {
                Close();
            }
        }
    }
}