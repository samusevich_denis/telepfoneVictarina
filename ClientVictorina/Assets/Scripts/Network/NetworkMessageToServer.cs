﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Victorina.Network
{
    [Serializable]
    public class NetworkMessageToServer
    {
        public TypeMessageToServer Type;
        public string Data;

        public NetworkMessageToServer(TypeMessageToServer type, string data)
        {
            Type = type;
            Data = data;
        }
    }
}