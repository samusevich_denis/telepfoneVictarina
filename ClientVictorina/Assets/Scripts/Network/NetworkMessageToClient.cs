﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Victorina.Network
{
    [Serializable]
    public class NetworkMessageToClient
    {
        public TypeMessageToClient Type;
        public string Data;

        public NetworkMessageToClient(TypeMessageToClient type, string data)
        {
            Type = type;
            Data = data;
        }
    }
}