﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using Victorina.Network;

namespace Victorina.Client
{
    public class ClientManager : MonoBehaviour
    {
        private Player player;
        public ClientState ClientState;
        public static ClientManager Instance;
        public static NetworkClient network;
        //public bool IsPlayGame => isPlayGame;
        //private bool isPlayGame;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            KeeperUIPanels.Instance.GetPanel(UIPanelType.NamePanel).ShowPanel();
        }

        public void SetName(string name, IPAddress address)
        {
            player = new Player(name, NetworkServer.LocalIPAddress().ToString());
            network = new NetworkClient();
            network.StartClient(player, address);

            Invoke("SendMessage", 5);
        }

        private void SendMessage()
        {
            var msg = ServerMessageHandler.GetMessage(player);
            network.SendMessageToServer(msg);
        }

        private void OnDestroy()
        {
            network.Close();
        }
    }
}