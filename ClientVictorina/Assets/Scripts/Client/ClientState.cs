﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Victorina.Client
{
    public abstract class ClientState
    {
        public abstract void UpdateState(ClientManager client);
        public abstract void ChangeState(ClientManager client);
    }
}

