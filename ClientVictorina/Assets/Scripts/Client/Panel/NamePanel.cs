﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using Victorina.UI;

namespace Victorina.Client
{
    public class NamePanel : UIPanel
    {
        [SerializeField] private Button buttonSetName;
        [SerializeField] private InputField inputFieldName;
        [SerializeField] private InputField inputFieldIP;
        protected override void Awake()
        {
            buttonSetName.onClick.AddListener(SetName);
            base.Awake();
        }

        private void SetName()
        {
            var name = inputFieldName.text;
            if (name.Equals(string.Empty, StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
            var ip = inputFieldIP.text;
            if (IPAddress.TryParse(ip, out IPAddress address))
            {
                ClientManager.Instance.SetName(name, address);
                ClosePanel();
                KeeperUIPanels.Instance.GetPanel(UIPanelType.WaitPanel).ShowPanel();
            }
        }

        private void OnDestroy()
        {
            buttonSetName.onClick.RemoveListener(SetName);
        }
    }
}