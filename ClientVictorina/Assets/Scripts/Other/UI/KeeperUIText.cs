﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UITextType
{
    None,
    NamePlayer_1,
    NamePlayer_2,
    NamePlayer_3,
    NamePlayer_4,
    NamePlayer_5,
    NamePlayer_6,
    NameMovie,
}

public class KeeperUIText : ObjectGeter<UIText, KeeperUIText>
{
    public UIText GetUIText(UITextType textType)
    {
        return GetObjectOnCondition((t) => t.TextType == textType);
    }
}
