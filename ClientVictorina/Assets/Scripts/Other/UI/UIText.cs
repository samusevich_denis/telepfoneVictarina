﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIText : MonoBehaviour
{
    [SerializeField] private UITextType textType = UITextType.None;
    public UITextType TextType { get => textType; }
    private Text text;

    protected virtual void Awake()
    {
        if (textType == UITextType.None)
        {
            Debug.LogWarning(gameObject.name + " is not assigned a UITextType ");
        }
        text = GetComponent<Text>();
        KeeperUIText.Instance.AddObject(this);
    }

    public virtual void SetText(string text)
    {
        this.text.text = text;
    }
}
