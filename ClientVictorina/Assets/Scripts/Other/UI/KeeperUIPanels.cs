﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Victorina.UI;

public enum UIPanelType
{
    None,
    StartGame,
    FinishGame,
    GameOver,
    MainPanel,

    NamePanel,
    WaitPanel,
}

public class KeeperUIPanels : ObjectGeter<UIPanel, KeeperUIPanels>
{
    public UIPanel GetPanel(UIPanelType panelType)
    {
        return GetObjectOnCondition((p) => p.PanelType == panelType);
    }
}
