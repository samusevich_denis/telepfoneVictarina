﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Victorina.UI;

public class StandartPanel : UIPanel
{
    [SerializeField] protected Button buttonRestart;
    [SerializeField] protected Button buttonBack;
    [SerializeField] private AudioSource audioSourceButton;

    protected override void Awake()
    {
        buttonRestart.onClick.AddListener(()=> {ClosePanel();} );
        buttonBack.onClick.AddListener(()=> {ClosePanel();});
        buttonRestart.onClick.AddListener(PlayAudioButton);
        buttonBack.onClick.AddListener(PlayAudioButton);
        base.Awake();
    }
    
    private void PlayAudioButton()
    {
        audioSourceButton.Play();
    }
    private void OnDestroy()
    {
        buttonBack.onClick.RemoveAllListeners();
        buttonRestart.onClick.RemoveAllListeners();
    }
}
