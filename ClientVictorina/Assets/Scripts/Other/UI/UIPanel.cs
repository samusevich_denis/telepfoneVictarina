﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Victorina.UI
{
    public abstract class UIPanel : MonoBehaviour
    {
        [SerializeField] private UIPanelType panelType = UIPanelType.None;
        public UIPanelType PanelType { get => panelType; }

        protected virtual void Awake()
        {
            if (panelType == UIPanelType.None)
            {
                Debug.LogWarning(gameObject.name + " is not assigned a UIPanelType ");
            }
            KeeperUIPanels.Instance.AddObject(this);
            gameObject.SetActive(false);
        }
        public virtual void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        public virtual void ShowPanel()
        {
            gameObject.SetActive(true);
        }
    }
}