﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Victorina.Admin
{
    public class ListPlayers : MonoBehaviour
    {
        [SerializeField] private List<Text> texts;

        public void AddPlayer(string name)
        {
            for (int i = 0; i < texts.Count; i++)
            {
                if (!texts[i].gameObject.activeSelf)
                {
                    texts[i].gameObject.SetActive(true);
                    texts[i].text = name + " готов";
                }
            }
        }
    }
}