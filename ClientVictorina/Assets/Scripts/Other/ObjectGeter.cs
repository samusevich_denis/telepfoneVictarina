﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGeter<T,Y> where T :class where Y :new()
{
    private static Y instance;
    public static Y Instance
    {
        get
        {
            if (instance != null)
            {
                return instance;
            }
            instance = new Y();
            return instance;
        }
    }

    protected List<T> objects = new List<T>();
    public  void AddObject(T element)
    {
        objects.Add(element);
    }

    public void RemoveObject(T element)
    {
        objects.Remove(element);
    }

    protected T GetObjectOnCondition(Func<T,bool> function)
    {
        for (int i = 0; i < objects.Count; i++)
        {
            if (function(objects[i]))
            {

                return objects[i];
            }
        }
        return null;
    }

    protected T GetRandomObjectOnCondition(Func<T, bool> function)
    {
        var objectsOnCondition = GetObjectsOnCondition(function);
        if (objectsOnCondition.Length ==0)
        {
            return null;
        }
        var indexRandom = UnityEngine.Random.Range(0, objectsOnCondition.Length);
        return objectsOnCondition[indexRandom];
    }

    protected T[] GetObjectsOnCondition(Func<T, bool> function)
    {
        var returnObjects = new List<T>();
        for (int i = 0; i < objects.Count; i++)
        {
            if (function(objects[i]))
            {

                returnObjects.Add(objects[i]);
            }
        }
        return returnObjects.ToArray();
    }

    protected void ExecuteForAllObject(Action<T> action)
    {

        for (int i = 0; i < objects.Count; i++)
        {
            action(objects[i]);
        }
    }
}
