﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="DataObject/MovieObject", fileName = "MovieObject.asset", order = 0)]
public class MovieData : DataObject
{
    public string Id;
    public Sprite Sprite;
    public string NameMovie;
}
