﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeeperDataObject : MonoBehaviour
{
    [SerializeField] List<DataObject> dataObjects;

    public DataObject GetRandomObject()
    {
        var random = Random.Range(0, dataObjects.Count-1);
        var dataObject = dataObjects[random];
        dataObjects.RemoveAt(random);
        return dataObject;
    }
}
